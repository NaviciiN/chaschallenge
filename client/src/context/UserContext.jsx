import { createContext, useState, useEffect, useContext } from 'react';
import axios from 'axios';

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [user, setUser] = useState();

  const getUser = async () => {
    try {
      const res = await axios.get('http://localhost:5173/api/users/current');
      setUser(res.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getUser();
  }, []);

  const login = async (email, password) => {
    try {
      await axios.post('http://localhost:5173/api/auth/login', {
        password,
        email,
      });

      await getUser();
    } catch (error) {
      console.log(error);
    }
  };

  const register = async (username, email, password) => {
    // Lägg till så användare kan välja profilbild
    try {
      await axios.post('http://localhost:5173/api/auth/register', {
        username,
        email,
        password,
      });
      await getUser();
    } catch (error) {
      console.error(error);
    }
  };

  const logout = async () => {
    try {
      await axios.post('http://localhost:5173/api/auth/logout');
      await getUser();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <UserContext.Provider value={{ user, login, register, logout }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUser must be used within AuthProvider');
  }
  return context;
};
