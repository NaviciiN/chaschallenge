import ReactDOM from 'react-dom/client';
import { UserProvider } from './context/UserContext';
import { BrowserRouter as Router, Routes, Route, useNavigate } from 'react-router-dom';
import {LandingPageLayout, LoginPageLayout, RegisterPageLayout, HomePageLayout} from './components'
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <UserProvider>
    <Router>
      <Routes>
        <Route path="/" element={<LandingPageLayout />} />
        <Route path="/login" element={<LoginPageLayout />} />
        <Route path="/register" element={<RegisterPageLayout />} />
        <Route path="/home" element={<HomePageLayout />} />
      </Routes>
    </Router>
  </UserProvider>
);
