function Input({ type, placeholder, id, name, value, setValue }) {
  const handleChange = (e) => {
    setValue(e.target.value)
  };

  return (
    <div>
      <input
        onChange={handleChange}
        type={type}
        id={id}
        name={name}
        value={value}
        placeholder={placeholder}
        className="w-[180px] text-center border-3 border-primary rounded-full text-2xl mb-6 px-4 py-2.5 font-primary drop-shadow-primary"
      />
    </div>
  );
}

export default Input;
