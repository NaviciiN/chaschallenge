// Footer kommer innehålla: Kontakt och länkar sociala medier
import React from "react";
import fblogo from "../../assets/icons/fbicon.svg";
import instagramlogo from "../../assets/icons/instagramicon.svg";
import linkedinlogo from "../../assets/icons/linkedinicon.svg";

function Footer() {
  return (
    <footer className="flex flex-row justify-between w-[360px] mb-2">
      <div>
        <h2 className="font-primary font-bold">kontakt</h2>
        <h3 className="font-primary">Mejladress@mejl.se</h3>
        <h4 className="font-primary">073-33223322</h4>
       
      </div>
        <div className="flex flex-row" >
            <img src={fblogo} alt="Facebook logga" />
            <img className="mx-2.5" src={linkedinlogo} alt="LinkedIn logga" />
            <img src={instagramlogo} alt="Instagram logga" />
        </div>
    </footer>
  );
}

export default Footer;