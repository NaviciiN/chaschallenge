import React, { useEffect, useState } from 'react';
import logo from '../../assets/logo.svg';
import { Footer, Button, Input } from '../index';
import { useUser } from '../../context/UserContext';
import { useNavigate } from 'react-router';

function LoginpageLayout() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { login, user } = useUser();
  const navigate = useNavigate();

  useEffect(() => {
    if (user) navigate('/home')
  }, [user])

  const handleClick = async () => {
    if (password && username) {
      await login(username, password);
    
    }
  };

  return (
    <div className="w-[390px] h-[844px] bg-bg-image flex flex-col justify-between items-center">
      <img src={logo} alt="Logo" />

      <div className="flex flex-col">
        <Input
          type={'text'}
          id={'username'}
          name={'username'}
          value={username}
          setValue={setUsername}
          placeholder={'Email'}
        />
        <Input
          type={'password'}
          id={'password'}
          name={'password'}
          value={password}
          setValue={setPassword}
          placeholder={'Lösenord'}
        />
        <Button onClick={handleClick} innerText={'Logga in'} />
      </div>
      <Footer />
    </div>
  );
}

export default LoginpageLayout;
