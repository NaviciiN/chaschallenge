
import { useNavigate } from "react-router";
import { useUser } from "../../context/UserContext";
import Button from "../Elements/Button";

const HomePageLayout = () => {
  const { logout} = useUser()
  const navigate = useNavigate()

  const handleLogout = async () => {
    try {
      await logout()
      navigate('/')
    } catch (error) {
      console.error(error);
    }
  }

  return <div className="grid place-items-center h-[100dvh]"><Button onClick={handleLogout} innerText='Logout' >Logout</Button></div>;
};

export default HomePageLayout;
