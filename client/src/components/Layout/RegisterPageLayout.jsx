import { useEffect, useState } from 'react';
import logo from '../../assets/logo.svg';
import { Footer, Button, Input } from '../index';
import { useUser } from '../../context/UserContext';
import { useNavigate } from 'react-router';

function RegisterPageLayout() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const { register, user } = useUser();

  const navigate = useNavigate()

  const handleClick = async () => {
    if (password && username && email) {
      await register(username, email, password);
    }
  };

  useEffect(() => {
    if (user) navigate('/home')
  }, [user])

  return (
    <div className="w-[390px] h-[844px] bg-bg-image flex flex-col justify-between items-center">
      <img src={logo} alt="Logo" />

      <div className="flex flex-col">
        <Input
          type={'text'}
          id={'username'}
          name={'username'}
          value={username}
          setValue={setUsername}
          placeholder={'Fullständigt namn'}
        />
        <Input
          type={'text'}
          id={'email'}
          name={'email'}
          value={email}
          setValue={setEmail}
          placeholder={'Mailadress'}
        />
        <Input
          type={'password'}
          id={'password'}
          name={'password'}
          value={password}
          setValue={setPassword}
          placeholder={'Lösenord'}
        />
        <Button
          onClick={handleClick}
          innerText={'Registrera'}
        />
      </div>
      <Footer />
    </div>
  );
}

export default RegisterPageLayout;
