import logo from '../../assets/logo.svg';
import { Button, Footer } from '../index';
import { useUser } from '../../context/UserContext';
import { useNavigate } from 'react-router';
import { useEffect } from 'react';



function LandingPageLayout() {
  const navigate = useNavigate()
  const {user} = useUser()

  useEffect(() => {
    if (user) navigate('/home')
  }, [user])
  

  return (
    <div className="w-[390px] h-[844px] bg-bg-image flex flex-col justify-between items-center">
      <img src={logo} alt="Logo" />

      <div className="flex flex-col">
        <Button innerText={'Logga in'} onClick={() => navigate('/login')} />
        <Button innerText={'Registrera'} onClick={() => navigate('/register')}/>
      </div>
      <Footer />
    </div>
  );
}

export default LandingPageLayout;
