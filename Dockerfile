FROM node:20-alpine3.16

WORKDIR /NaviciiN/chaschallenge/server

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY server/package.json package.json
COPY client/package.json package.json
RUN npm install

COPY . .

EXPOSE 3000
CMD [ "npm", "start" ]
