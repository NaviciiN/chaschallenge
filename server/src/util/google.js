const { google } = require('googleapis');

const client = new google.auth.OAuth2(
  process.env.CLIENT_ID,
  process.env.CLIENT_SECRET,
  process.env.NODE_ENV === 'dev'
    ? 'http://localhost:3000/api/auth/google/dev-callback'
    : 'http://localhost:3000/api/auth/google/callback'
);

const scopes = [
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/userinfo.profile',
];

const authUrl = client.generateAuthUrl({
  access_type: 'offline',
  scope: scopes,
});

const oauth2 = google.oauth2({ version: 'v2', auth: client });

module.exports = { client, authUrl, oauth2 };
