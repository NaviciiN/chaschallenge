const User = require('../models/userModel');
const crypto = require('crypto');
const { authUrl, client, oauth2 } = require('../util/google');

const registerUser = async (req, res) => {
  try {
    const { username, email, password, photo_url } = req.body;

    const userExisits = await User.findOne({ email });
    if (userExisits)
      return res.status(409).json({ message: 'Email already in use' });

    const verificationToken = crypto.randomBytes(20).toString('hex');

    const user = await User.create({
      email,
      username,
      password,
      verificationToken,
      photo_url,
    });

    req.session.userId = user._id;
    res.status(201).json({ message: 'Registration successfull' });
  } catch (err) {
    console.error(err);
    res.status(500).json('Internal server error');
  }
};

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });
    if (!user) return res.status(404).json({ message: 'User not found' });

    if (!user.password)
      return res.status(403).json({ message: 'User does not have a password' });

    const match = await user.comparePassword(password);
    if (!match) return res.status(401).json({ message: 'Incorrect password' });

    req.session.userId = user._id;
    res.status(200).json({ message: 'Login successful' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const logoutUser = async (req, res) => {
  req.session.destroy();
  res.status(200).json({ message: 'Logout successful' });
};

const verifyUserEmail = async (req, res) => {
  try {
    const { token } = req.query;

    const user = await User.findOne({ verificationToken: token });
    if (!user) return res.status(401).json({ message: 'Invalid token' });

    user.isVerified = true;
    user.verificationToken = undefined;
    await user.save();

    res.status(200).json({ message: 'Email verified successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const updateUserEmail = async (req, res) => {
  try {
    const { token } = req.query;

    const user = await User.findOne({ updateToken: token });
    if (!user) return res.status(401).json({ message: 'Invalid token' });

    user.email = user.new_email;
    user.new_email = undefined;
    user.updateToken = undefined;

    await user.save();

    res.status(200).json({ message: 'Email updated successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const google = async (req, res) => {
  res.status(200).json(authUrl);
};

const googleCallback = async (req, res) => {
  try {
    const { code } = req.query;

    const { tokens } = await client.getToken(code);
    client.setCredentials(tokens);

    const userInfo = await oauth2.userinfo.get();

    const profile = userInfo.data;
    const email = profile.email;

    let user = await User.findOne({ email });

    if (user) {
      if (!user.isVerified) {
        user.isVerified = profile.verified_email;
        user.verificationToken = undefined;
        await user.save();
      }
    } else {
      user = await User.create({
        email,
        username: profile.given_name,
        photo_url: profile.picture,
        isVerified: profile.verified_email,
      });
    }

    req.session.userId = user._id;

    if (process.env.NODE_ENV === 'dev') return res.status(200).json('Success');
    res.redirect('http://localhost:5173');
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal server error' });
  }
};

const googleDevCallback = async (req, res) => {
  const { code } = req.query;

  res.status(200).json(code);
};

module.exports = {
  registerUser,
  loginUser,
  logoutUser,
  verifyUserEmail,
  updateUserEmail,
  google,
  googleCallback,
  googleDevCallback,
};
