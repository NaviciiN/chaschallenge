const Post = require("../models/postModel");
const Review = require("../models/reviewModel");

const addReview = async (req, res) => {
  try {
    const { rating, description } = req.body;
    const { postId } = req.query; //Receiver
    const { userId } = req.session; //Submitter

    /* Getting users involved */

    // Check if the user has already reviewed this post
    const existingReview = await Review.findOne({
      post: postId,
      submitter: userId,
    });

    if (existingReview)
      return res
        .status(400)
        .json({ message: "You have already reviewed this post" });

    // Get the post and validate its existence
    const post = await Post.findById(postId);
    if (!post) return res.status(404).json({ message: "Post not found" });

    // Check if the user is the poster
    if (post.poster.toString() === userId)
      return res.status(400).json({ message: "Cannot review yourself" });

    // Check if the user is involved in the purchase
    const isBuyer = post.accepted_requester.toString() === userId;
    const isSeller = post.poster.toString() === userId;
    if (!isBuyer && !isSeller)
      return res
        .status(400)
        .json({ message: "You are not involved in this purchase" });

    // Create the review
    await Review.create({
      post: post._id,
      receiver: isBuyer ? post.poster : post.accepted_requester,
      submitter: userId,
      description: description,
      rating: rating,
      type: isBuyer ? "seller" : "buyer",
    });

    /* Creating the review */
    res.status(200).json("Successfull review creation");
  } catch (err) {
    console.error(err);
    res.status(500).json("Internal server error");
  }
};

const getUserReviews = async (req, res) => {
  try {
    const { userId } = req.query;

    // Find all reviews submitted by the user
    // Find all reviews where the receiver is the user and populate the post field
    const sellerReviews = await Review.find({ receiver: userId }).populate({
      path: "post",
      match: { poster: userId },
    });

    // Find all reviews where the receiver is the user and the post's accepted_requester is the user, and populate the post field
    const buyerReviews = await Review.find({ receiver: userId }).populate({
      path: "post",
      match: { poster: { $ne: userId } },
    });

    const submittedReviews = await Review.find({ submitter: userId });

    res.status(200).json({ sellerReviews, buyerReviews, submittedReviews });
  } catch (err) {
    console.error(err);
    res.status(500).json("Internal server error");
  }
};

module.exports = { addReview, getUserReviews };
