const Chat = require('../models/chatModel');
const Post = require('../models/postModel');

const getUserChats = async (req, res) => {
  try {
    const { userId } = req.session;

    const chats = await Chat.find({ participants: { $in: userId } });

    res.status(200).json({ chats });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const offerPickup = async (req, res) => {
  try {
    const { postId } = req.query;
    const { userId } = req.session;

    const post = await Post.findById(postId);

    if (post.accepted_requester)
      return res.status(400).json('Pickup already accepted');

    if (post.poster.toString() === userId)
      return res
        .status(400)
        .json({ message: 'Cannot request a pickup on your own post' });

    if (post.requesters.includes(userId))
      return res
        .status(400)
        .json({ message: 'You already requested a pickup' });

    const requesters = post.requesters
      ? [...post.requesters, userId]
      : [userId];

    await Post.findOneAndUpdate(
      { _id: postId },
      {
        requesters,
      },
      { new: true }
    );

    await Chat.create({
      name: post.title,
      participants: [userId, post.poster],
      topic: post._id,
    });

    res.status(200).json({ message: 'Offer successful' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const acceptPickup = async (req, res) => {
  try {
    const { chatId } = req.query;
    const { userId } = req.session;

    const chat = await Chat.findById(chatId);

    const post = await Post.findById(chat.topic);

    if (post.accepted_requester)
      return res.status(400).json({ message: 'Pickup already accepted' });

    const posterId = chat.participants[1];
    const requesterId = chat.participants[0];

    if (!(posterId.toString() === userId))
      return res.status(401).json({ message: 'Unauthorized' });

    await Post.findOneAndUpdate(
      { _id: chat.topic },
      {
        requesters: [],
        accepted_requester: requesterId,
      }
    );

    res.status(200).json({ message: 'Request accepted successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const declinePickup = async (req, res) => {
  try {
    const { chatId } = req.query;
    const { userId } = req.session;

    const chat = await Chat.findById(chatId);

    const requesterId = chat.participants[0];
    const posterId = chat.participants[1];

    if (!(posterId.toString() === userId))
      return res.status(401).json({ message: 'Unauthorized' });

    await Post.findOneAndUpdate(
      { _id: chat.topic },
      { $pull: { requesters: requesterId } }
    );

    await Chat.findOneAndDelete({ _id: chat._id });
    res.status(200).json({ message: 'Declined pickup successfully' });
  } catch (err) {
    console.error(err);
    res.status(200).json({ message: 'Internal server error' });
  }
};

module.exports = {
  getUserChats,
  offerPickup,
  acceptPickup,
  declinePickup,
};
