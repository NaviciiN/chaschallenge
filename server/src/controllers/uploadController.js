const cloudinary = require('../util/cloudinary');

const uploadPostPictures = async (req, res) => {
  try {
    const files = req.files;
    const fileArray = [];

    const asyncUpload = async (file) => {
      return new Promise((resolve, reject) => {
        try {
          cloudinary.uploader
            .upload_stream(
              { resource_type: 'auto', folder: 'posts' },
              (err, result) => {
                if (err) throw err;
                console.log(result);
                fileArray.push(result.secure_url);
                resolve();
              }
            )
            .end(file.buffer);
        } catch (err) {
          console.error(err);
          reject();
        }
      });
    };

    for (const file in files) await asyncUpload(files[file]);

    res.status(200).json({ photo_urls: fileArray });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const uploadProfilePicture = async (req, res) => {
  try {
    const file = req.file;

    cloudinary.uploader
      .upload_stream(
        { resource_type: 'auto', folder: 'profile' },
        (err, result) => {
          if (err) throw new err();
          console.log(result.secure_url);
          res.status(200).json({ url: result.secure_url });
        }
      )
      .end(file.buffer);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports = { uploadPostPictures, uploadProfilePicture };
