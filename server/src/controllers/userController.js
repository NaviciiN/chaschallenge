const User = require('../models/userModel');
const Post = require('../models/postModel');
const crypto = require('crypto');

const deleteCurrentUser = async (req, res) => {
  try {
    const userId = req.session.userId;
    await User.findByIdAndDelete(userId);
    req.session.destroy();
    res.status(200).json({ message: 'User deleted successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const getCurrentUser = async (req, res) => {
  try {
    const userId = req.session.userId;

    if (!userId) return res.status(200).json(null);

    const user = await User.findById(userId);

    const userInfo = {
      id: user._id,
      username: user.username,
      photo_url: user.photo_url,
      email: user.email,
    };

    res.status(200).json(userInfo);
  } catch (err) {
    console.error(err);
    res.status(500).json('Internal server error');
  }
};

const editCurrentUser = async (req, res) => {
  try {
    const { username, email, old_password, new_password, photo_url } = req.body;

    const userId = req.session.userId;

    const user = await User.findById(userId);
    if (!user) return res.status(404).json({ message: 'User not found' });

    if (email) {
      if (!user.password)
        return res
          .status(403)
          .json({ message: 'You must set a password before changing email' });

      if (email === user.email)
        return res.status(400).json("New email can't be the same as old email");

      if (!user.isVerified) return res.status(401).json('Email not verified');

      const emailExists = await User.findOne({ email });
      if (emailExists)
        return res.status(409).json({ message: 'Email already in use' });

      const newEmailExists = await User.findOne({ new_email: email });
      if (newEmailExists)
        return res.status(409).json({ message: 'Email already in use' });

      const updateToken = crypto.randomBytes(20).toString('hex');
      user.updateToken = updateToken;
      user.new_email = email;
    }

    if (username) {
      if (username === user.username)
        return res
          .status(400)
          .json("New username can't be the same as old username");

      user.username = username;
    }

    if (photo_url) {
      if (photo_url && photo_url === user.photo_url)
        return res.status(400).json("New photo can't be the same as old photo");

      user.photo_url = photo_url;
    }

    if (new_password && !user.password) {
      user.password = new_password;
    } else if (new_password && old_password) {
      if (new_password && new_password === old_password)
        return res
          .status(400)
          .json("New password can't be the same as old password");

      const match = await user.comparePassword(old_password);
      if (!match)
        return res.status(401).json({ message: 'Incorrect password' });

      user.password = new_password;
    }

    await user.save();
    res.status(200).json('Success');
  } catch (err) {
    console.error(err);
    res.status(500).json(' Internal server error');
  }
};

const getUserProfile = async (req, res) => {
  try {
    //Get user profile
    const userId = req.params.id;

    const user = await User.findById(userId);

    const activePosts = await Post.find({ poster: userId, isCompleted: false });
    const completedPosts = await Post.find({
      poster: userId,
      isCompleted: true,
    });

    const userInfo = {
      username: user.username,
      photo_url: user.photo_url,
      activePosts: activePosts.length,
      completedPosts: completedPosts.length,
    };

    //Get posts associated with user

    res.status(200).json({ userInfo });
  } catch (err) {
    console.error(err);
    res.status(500).json('Internal server error');
  }
};


module.exports = {
  getUserProfile,
  getCurrentUser,
  editCurrentUser,
  deleteCurrentUser,
};
