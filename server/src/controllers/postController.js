const Post = require('../models/postModel');

const getPosts = async (req, res) => {
  try {
    const { categories, city, price_range, search } = req.body;

    const filter = {};

    if (categories) filter.category = { $in: categories };

    if (city) filter.city = city;

    if (price_range) {
      if (price_range.min <= 0 || !price_range.min) price_range.min = 1;
      if (price_range.max <= 0 || !price_range.max) price_range.max = 999999;
      filter.price = { $gte: price_range.min, $lte: price_range.max };
    }

    if (search) {
      const searchRegex = new RegExp(search, 'i');
      filter.$or = [{ title: searchRegex }, { description: searchRegex }];
    }

    const filteredPosts = await Post.find(filter);

    res.status(200).json({ filteredPosts });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const createPost = async (req, res) => {
  try {
    const {
      title,
      description,
      photo_url,
      price,
      city,
      pickup_dates,
      pickup_times,
      category,
    } = req.body;

    const poster = req.session.userId;

    await Post.create({
      title,
      description,
      photo_url,
      poster,
      price,
      city,
      pickup_dates,
      pickup_times,
      category,
    });

    res.status(201).json({ message: 'Post created successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const editPost = async (req, res) => {
  try {
    const { postId } = req.query;
    const { userId } = req.session;

    const post = await Post.findById(postId);

    if (!post) return res.status(404).json({ message: 'Post not found' });

    if (post.poster.toString() !== userId)
      return res.status(403).json({ message: 'Unauthorized' });

    const updateFields = req.body;

    await Post.updateOne({ _id: postId }, updateFields, { new: true });

    res.status(200).json({ message: 'Post updated successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const deletePost = async (req, res) => {
  try {
    const { postId } = req.query;
    const { userId } = req.session;

    const deletedPost = await Post.findOneAndDelete({
      _id: postId,
      poster: userId,
    });

    if (!deletedPost)
      return res.status(404).json({ message: 'Post not found' });

    res.status(200).json({ message: 'Post deleted successfully' });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const getCurrentUserPosts = async (req, res) => {
  try {
    const { userId } = req.session;
    const posts = await Post.find({ poster: userId });
    res.status(200).json(posts);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports = {
  createPost,
  editPost,
  deletePost,
  getCurrentUserPosts,
  getPosts,
};
