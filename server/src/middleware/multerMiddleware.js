const multer = require('multer');

const storage = multer.memoryStorage();
const upload = multer({ storage });

const multerSingleMiddleware = (req, res, next) => {
  try {
    upload.single('file')(req, res, (err) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Error uploading file' });
      }
      next();
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

const multerMultipleMiddleware = (req, res, next) => {
  try {
    upload.array('files', 4)(req, res, (err) => {
      if (err) {
        console.error(err);
        return res.status(500).json({ message: 'Error uploading file' });
      }
      next();
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports = { multerSingleMiddleware, multerMultipleMiddleware };
