const mongoose = require('mongoose');

const messageSchema = mongoose.Schema(
  {
    sender: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
    content: { type: String, trim: true },
    chat: { type: mongoose.Schema.Types.ObjectId, ref: 'chats' },
    unread: { type: mongoose.Schema.Types.ObjectId, ref: 'users' },
    readDate: { type: Date },
  },
  { timestamps: true }
);

module.exports = mongoose.model('messages', messageSchema);
