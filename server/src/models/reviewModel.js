const mongoose = require("mongoose");

const validTypes = ["seller", "buyer"];

const reviewSchema = new mongoose.Schema({
  post: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "posts",
    required: true,
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
    required: true,
  },
  submitter: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users",
    required: true,
  },
  description: {
    type: String,
  },
  rating: { type: Number, min: 0, max: 5, required: true },
  type: {
    type: String,
    required: true,
    enum: validTypes,
  },
});

module.exports = mongoose.model("reviews", reviewSchema);
