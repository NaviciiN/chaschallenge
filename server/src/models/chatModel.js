const mongoose = require('mongoose');

const chatSchema = new mongoose.Schema(
  {
    name: { type: String, trim: true },
    participants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'users' }],
    topic: { type: mongoose.Schema.Types.ObjectId, ref: 'posts' },
    latestMessage: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'messages',
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('chats', chatSchema);
