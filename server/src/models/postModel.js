const mongoose = require('mongoose');

const validCategories = ['recycle', 'donate', 'bottle'];

const PostSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    photo_urls: [
      {
        type: String,
        required: true,
      },
    ],
    poster: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    price: {
      type: Number,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    pickup_dates: [
      {
        type: Date,
        required: true,
      },
    ],
    pickup_times: [
      {
        type: String,
        required: true,
      },
    ],
    requesters: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
      },
    ],
    accepted_requester: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
    },
    isCompleted: {
      type: Boolean,
      default: false,
    },
    category: {
      type: String,
      required: true,
      enum: validCategories,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('posts', PostSchema);
