// Importing the necessary modules
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

// Defining the userSchema using Mongoose
const userSchema = new mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    new_email: { type: String },
    password: { type: String },
    username: { type: String, required: true },
    photo_url: { type: String, default: 'http://fulbild.com' },
    isVerified: { type: Boolean, default: false },
    verificationToken: { type: String },
    updateToken: { type: String },
  },
  { timestamps: true } // timestamps will be automatically added for createdAt and updatedAt fields
);

// Before saving the user data to the database, we need to hash the password using bcrypt
userSchema.pre('save', async function (next) {
  const user = this;
  if (!user.isModified('password')) next(); // if password is not modified, proceed to next middleware
  const salt = await bcrypt.genSalt(10); // generate a salt with cost factor 10
  const hash = await bcrypt.hash(user.password, salt); // hash the password with the generated salt
  user.password = hash; // store the hashed password in the user object
  next(); // proceed to next middleware
});

// Adding a method to the userSchema to compare the plain password with the hashed password
userSchema.methods.comparePassword = async function (password) {
  return await bcrypt.compare(password, this.password); // compare the password with the stored hashed password
};

// Exporting the userSchema as a mongoose model named 'users'
module.exports = mongoose.model('users', userSchema);
