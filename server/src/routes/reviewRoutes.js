const express = require("express");
const controller = require("../controllers/reviewController");
const { authenticateUser } = require("../middleware/authMiddleware");

const router = express.Router();

router
  .route("/")
  .post(authenticateUser, controller.addReview)
  .get(controller.getUserReviews);

module.exports = router;
