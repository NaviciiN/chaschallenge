const express = require('express');
const controller = require('../controllers/userController');
const { authenticateUser } = require('../middleware/authMiddleware');

const router = express.Router();

router
  .route('/current')
  .get(controller.getCurrentUser)
  .put(authenticateUser, controller.editCurrentUser)
  .delete(authenticateUser, controller.deleteCurrentUser)

//Ska vi ha autentisering på denna endpoint?
router.get('/:id', controller.getUserProfile);

module.exports = router;
