const express = require('express');
const controller = require('../controllers/authController');
const { authenticateUser } = require('../middleware/authMiddleware');

const router = express.Router();

router.post('/register', controller.registerUser);
router.post('/login', controller.loginUser);
router.post('/logout', authenticateUser, controller.logoutUser);
router.post('/verify-email', controller.verifyUserEmail);
router.post('/verify-new-email', controller.updateUserEmail);

if (process.env.NODE_ENV === 'dev')
  router.get('/google/dev-callback', controller.googleDevCallback);

router.get('/google', controller.google);
router.get('/google/callback', controller.googleCallback);

module.exports = router;
