const express = require('express');
const controller = require('../controllers/chatController');
const { authenticateUser } = require('../middleware/authMiddleware');

const router = express.Router();

router
  .route('/')
  .get(authenticateUser, controller.getUserChats)
  .post(authenticateUser, controller.offerPickup);

router.route('/accept').post(authenticateUser, controller.acceptPickup);
router.route('/decline').post(authenticateUser, controller.declinePickup);

module.exports = router;
