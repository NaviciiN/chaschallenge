const express = require('express');
const controller = require('../controllers/postController');
const { authenticateUser } = require('../middleware/authMiddleware');

const router = express.Router();

router
  .route('/')
  .post(authenticateUser, controller.createPost)
  .get(authenticateUser, controller.getPosts);

router.route('/user').get(authenticateUser, controller.getCurrentUserPosts);

router
  .route('/:id')
  .put(authenticateUser, controller.editPost)
  .delete(authenticateUser, controller.deletePost);

module.exports = router;
