const express = require('express');
const controller = require('../controllers/uploadController');
const {
  multerSingleMiddleware,
  multerMultipleMiddleware,
} = require('../middleware/multerMiddleware');

const router = express.Router();

router.post('/post', multerMultipleMiddleware, controller.uploadPostPictures);
router.post(
  '/profile',
  multerSingleMiddleware,
  controller.uploadProfilePicture
);

module.exports = router;
