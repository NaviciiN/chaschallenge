require('dotenv').config();
const express = require('express');
const connect = require('./util/db');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const userRoutes = require('./routes/userRoutes');
const postRoutes = require('./routes/postRoutes');
const authRoutes = require('./routes/authRoutes');
const uploadRoutes = require('./routes/uploadRoutes');
const reviewRoutes = require('./routes/reviewRoutes');
const chatRoutes = require('./routes/chatRoutes');
const morgan = require('morgan');
const Message = require('./models/messageModel');
const Chat = require('./models/chatModel');
const cors = require('cors');
const PORT = process.env.PORT || 3000;

const app = express();

connect();

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store: MongoStore.create({
      mongoUrl: process.env.MONGODB_URL,
    }),
    cookie: { maxAge: 31 * 24 * 60 * 60 * 1000 },
  })
);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan('dev'))

app.use('/api/users', userRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/posts', postRoutes);
app.use('/api/upload', uploadRoutes);
app.use('/api/review', reviewRoutes);
app.use('/api/chat', chatRoutes);

const server = app.listen(PORT, console.log(`Server started on PORT ${PORT}`));

const socket = require('socket.io')(server);

socket.on('connection', (socket) => {
  console.log('Connected to socket.io');

  socket.on('join', (chat) => {
    socket.join(chat);
    socket.to(chat).emit('user-connected');
  });

  socket.on('new-message', async ({ sender, content, chat }) => {
    try {
      const currentChat = await Chat.findById(chat);
      const reciever = currentChat.participants.find(
        (participant) => participant.toString() !== sender.toString()
      );

      const message = await Message.create({
        sender,
        content,
        chat,
        unread: reciever,
      });

      await Chat.findOneAndUpdate(
        { _id: chat },
        {
          latestMessage: message._id,
        }
      );

      socket.to(chat).emit('message-received', message.content);
    } catch (err) {
      console.error(err);
    }
  });

  socket.on('user-typing', ({ chat, isTyping }) => {
    socket.to(chat).emit('user-typing', isTyping);
  });

  socket.on('read-message', async ({ chat, message }) => {
    await Message.findOneAndUpdate(
      { _id: message },
      {
        $unset: { unread: 1 },
        readDate: Date.now(),
      }
    );
    socket.to(chat).emit('read-message', message);
  });
});
