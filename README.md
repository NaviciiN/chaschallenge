## Introduction

Welcome to the Chas challenge! In this project, our team consisting of full-stack developers, UI/UX designers, and a DevOps engineer has created a web application.

In this readme, you will find instructions on setting up the .env file for the backend and running the backend of the application.

## Setting up .env for the backend

Before you can run the backend of the application, you need to set up the `.env` file with the necessary configuration variables. Follow these steps to set up the `.env` file:

1. Create a `.env` file in the server root directory.
2. Add the following Firebase configuration variables to the `.env` file, replacing the empty values with your own Firebase project's credentials:

```
NODE_ENV="" # dev | prod
MONGODB_URL="" # mongodb connection string
PORT="" port # which the server should run on
SESSION_SECRET="" # secret use to protect sessions
CLIENT_ID="" # google oath2 client id
CLIENT_SECRET="" # google oath2 client secret
CLOUDINARY_NAME="dejp78tms" # cloudinary name
CLOUDINARY_KEY="497973777734445" # cloudinary API key
CLOUDINARY_SECRET="IZaJB2euwd2R5Rz0cASbsPzztzA" # key secret
```

## Running the backend

Once you have set up the `.env` file, you can run the backend of the application by following these steps:

1. Change current directory to `server`

```
cd server
```

2. Install project dependencies.

```
npm install
```

3. Start the development server.

```
npm run dev
```

That's it! With these simple steps, you can get the backend of the Chas application up and running.

## Team Members

The team members who worked on this project are:

### Fullstack

- Alexander Fallstrom
- Benjamin Sundell
- Emelie Barrington
- Gustav Johansson
- Latifa Rohani

### DevOps

- Navid Mircoram

### UI/UX

- Jessika Wiberg
- Maher Hezam